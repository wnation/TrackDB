package com.wnation.trackdb.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;


public class HomeActivity extends ActionBarActivity {

    private Spinner select_item;
    private Button btnSubmit;
    private AutoCompleteTextView mAutoComplete;
    private ArrayList<String> dictionary = new ArrayList<String>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //dummy values until I can get real entries
        dictionary.add("will nation");
        dictionary.add("texas");

        addItemsOnSpinner();
        addListenerOnButton();
        setAutoCompleteText();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addListenerOnButton() {
        select_item = (Spinner) findViewById(R.id.search_spinner);
        btnSubmit = (Button) findViewById(R.id.search);

        // redirect to specified search
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ID", "nothing");
            }
        });
    }



    public void addItemsOnSpinner() {
        select_item = (Spinner) findViewById(R.id.search_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.search_items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    public void setAutoCompleteText() {
        mAutoComplete = (AutoCompleteTextView) findViewById(R.id.search_field);


    }

}
